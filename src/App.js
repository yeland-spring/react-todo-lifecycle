import React, {Component} from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isShow: false
    };
    this.switchShowAndHide = this.switchShowAndHide.bind(this);
    this.refreshPage = this.refreshPage.bind(this);
  }

  render() {
    return (
      <div className="App">
        <div className="show">
          <button className="exist" onClick={this.switchShowAndHide}>{this.state.isShow?"hide":"show"}</button>
          <button className="exist" onClick={this.refreshPage}>Refresh</button>
        </div>
        <TodoList isShow={this.state.isShow}/>
      </div>
    );
  }

  switchShowAndHide() {
    this.setState({isShow: !this.state.isShow});
  }

  refreshPage() {
    location.reload();
  }
}

export default App;