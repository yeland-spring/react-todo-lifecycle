import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      index: 1,
      items: []
    };
    this.addItem = this.addItem.bind(this);
    console.log("constructor");
  }

  render() {
    if (this.props.isShow) {
      return (
        <div className="todo-list">
          <button className="add" onClick={this.addItem}>Add</button>
          <div>{this.state.items.map(item => <p>{item}</p> )}</div>
        </div>
      )
    }
    return (
      <div/>
    );
  }

  addItem() {
    this.setState({items: [...this.state.items, `List Tittle ${this.state.index}`]});
    this.setState({index: ++this.state.index});
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }
}

export default TodoList;

